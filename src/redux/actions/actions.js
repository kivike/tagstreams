import axios from 'axios';

const API_URL = process.env.NODE_ENV === 'production' ? '/api/': 'http://localhost:4444/api/';

export function getStreamables(data) {
    return (dispatch => {
        axios.get(`${API_URL}streamable`, data).then((res) => {
            if (res.status === 200) {
                let streamables = res.data;
                console.log(streamables);
                dispatch({type: 'GET_STREAMABLES', streamables});
            } else {
                console.error(res.data)
            }
        })
    })
}

export function submitStreamable(data) {
    return (dispatch => {
        axios.post(`${API_URL}streamable/submit`, data).then((res) => {
            console.log(res);
        });
    })
}

export function setStreamableFrameUrl(url) {
    return (dispatch => {
        console.log('new url: ' + url);
        dispatch({type: 'SET_FRAME_URL', url});
    });
}

export function openReportForm(url) {
    console.log('DISPATCHH');
    return (dispatch => {
        dispatch({type: 'SET_REPORT_URL', url})
    });
}
export function closeReportForm() {
    return (dispatch => {
        dispatch({type: 'SET_REPORT_URL', url: ''})
    });
}