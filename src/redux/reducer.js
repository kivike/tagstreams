import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import streamables from './reducers/streamables';

export default combineReducers({
    streamables,
    router: routerReducer
});