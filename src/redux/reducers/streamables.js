const initialState = {
    streamables: [],
    frameUrl: ''
}

export default (state = initialState, action) => {
    switch(action.type) {
        case 'GET_STREAMABLES':
            return {
                ...state,
                streamables: action.streamables
            }
        case 'SET_FRAME_URL':
            console.log(action);
            return {
                ...state,
                frameUrl: action.url
            }
        case 'SET_REPORT_URL':
            console.log(action);
            return {
                ...state,
                reportUrl: action.url
            }
        default:
            return state;
    }
}