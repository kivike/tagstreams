import React, { Component } from 'react';
import '../Styles/App.css';
import UploadForm from './UploadForm';
import ReportForm from './Streamable/ReportForm';
import StreamableList from './Streamable/List';
//import StreamableFrame from './Streamable/Frame.js';

class App extends Component {
  render() {
    return (
      <div className="App no-select">
        <header className="App-header">
        </header>
        <UploadForm />
        <ReportForm />
        <div className="streamable-container">
          <StreamableList />
          {/*<StreamableFrame />*/}
        </div>
      </div>
    );
  }
}

export default App;