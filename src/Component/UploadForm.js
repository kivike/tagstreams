import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { submitStreamable } from '../redux/actions/actions';

const UPLOAD_URL_BASE = process.env.NODE_ENV === 'production' ? '/' : 'http://localhost:4444/'
const STREAMABLE_UPLOAD_URL = UPLOAD_URL_BASE + 'api/streamable/submit';

const MIN_LOAD_TIME = 500;

class UploadForm extends Component {
    constructor() {
        super();

        this.state = {
            loading: false,
            sent: false
        }

        this.submitForm = this.submitForm.bind(this);
        this.validatePassword = this.validatePassword.bind(this);
        this.onUploadComplete = this.onUploadComplete.bind(this);
    }
    submitForm(e) {
        e.preventDefault();

        if (/*!this.validatePassword() || */!document.getElementById('clip-name').value.trim().length) {
            return;
        }

        this.setState({
            loading: 'waiting'
        });

        let uploadStartTime = getTimestamp();

        let uploader = document.getElementById('clip-uploader').value.trim();

        if (uploader) {
            document.cookie = "uploader=" + uploader;
        }
        const formData = new FormData();
        formData.append('url', document.getElementById('clip-url').value.trim());
        formData.append('name', document.getElementById('clip-name').value.trim());
        formData.append('uploader', uploader);
        //formData.append('tags', document.getElementById('clip-tags').value);
        //formData.append('password', document.getElementById('form-password').value.trim());

        axios.post(`${STREAMABLE_UPLOAD_URL}`, formData).then((res) => {
            this.setState({
                sent: true
            });
            this.onUploadComplete(uploadStartTime, true);
        }).catch((err) => {
            console.log(err);
            this.onUploadComplete(uploadStartTime, false);
        });
    }
    componentDidMount() {
        let cookieUploader = getCookie('uploader');

        if (cookieUploader) {
            document.getElementById('clip-uploader').value = cookieUploader;
        }
    }
    onUploadComplete(uploadStartTime, result) {
        let uploadFinishTime = getTimestamp();

        let timeLeft = MIN_LOAD_TIME - (uploadFinishTime - uploadStartTime);
        timeLeft = Math.max(0, timeLeft);

        let self = this;

        setTimeout(function() {
            self.setState({loading: result ? 'success' : 'failed'});

            if (true === result) {
                document.getElementById('clip-url').value = '';
                document.getElementById('clip-name').value = '';
            }

            setTimeout(function() {
                self.setState({loading: ''})
            }, 1000);
        }, timeLeft);
    }
    validatePassword() {
        return document.getElementById('form-password').value.length > 0;
    }
    render() {
        return (
            <div className="form-container upload-form-container">
                <div className="overlay">
                    { this.state.loading ? <div className={"loader status-" + this.state.loading}/> : null }
                    { this.state.loading == 'waiting' ? <div className="spinner"/> : null}
                </div>
                
                <form id="upload-form" action="" onSubmit={this.submitForm}>
                    <div className="form-row">
                        <label>Streamable</label>
                        <input type="text" placeholder="https://streamable.com/ABC123" name="url" id="clip-url" />
                    </div>
                    <div className="form-row">
                        <label>Clipin nimi</label>
                        <input type="text" placeholder="" name="clipName" id="clip-name" />
                    </div>
                    {/*<div className="form-row">
                        <label>Tagit</label>
                        <input type="text"placeholder="csgo,awp" name="clipTags" id="clip-tags" />
                    </div>*/}
                    <div className="form-row">
                        <label>Lähettäjä</label>
                        <input type="text" placeholder="(vapaaehtoinen)" name="clipUploader" id="clip-uploader" />
                    </div>
                    <button type="submit" id="submit-btn">Lähetä</button>
                </form>
            </div>
        );
    }

}

const getCookie = function(name) {
    var cookiestr = RegExp("" + name + "[^;]+").exec(document.cookie);
    return cookiestr ? cookiestr.toString().replace(/^[^=]+./,"") : "";
}
const getTimestamp = function() {
    return (new Date()).getTime();
}
const mapStateToProps = state => {
    return {

    }
}

export default connect(mapStateToProps, { submitStreamable })(UploadForm);