import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setStreamableFrameUrl } from '../../redux/actions/actions';
import { openReportForm } from '../../redux/actions/actions';

const nameToColor = function(name) {
    if (!name) {
        return '#fff';
    }
    let charsToUse = 3;
    let colorStr = '#';

    for (let i = 0; i < charsToUse; i++) {
        let char = name.charCodeAt(i);
        let hex = 'f';

        if (char) {
            hex = (6 + char % 10).toString(16);
        }
        colorStr += hex;
    }
    return colorStr;
}
const formatTime = function(time_str) {
    let time = Date.parse(time_str);
    let currentTime = new Date();

    let diff = currentTime - time;
    let diffDays = diff / (1000 * 3600 * 24);
    
    if (diffDays < 1) {
        return 'Tänään';
    } else if (diffDays < 2) {
        return 'Eilen';
    } else if (diffDays < 15) {
        return Math.floor(diffDays) + " päivää sitten";
    }
    return time_str;
}
class ListItem extends Component {
    constructor() {
        super();

        this.state = {
            mobile: window.innerWidth <= 800
        }
        this.embedItem = this.embedItem.bind(this);
        this.openReportForm = this.openReportForm.bind(this);
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }
    render() {
        const uploaderColor = nameToColor(this.props.uploader);

        return (
            <div className="table-row streamable-container">
                <a className="table-row streamable-container" href={this.props.url}>
                    <div className="table-cell col col-name">
                        {this.props.name}
                    </div>
                    {/*<td className="col streamable-tags">
                        {this.props.tags}
                    </td>*/}
                    <div className="table-cell col col-uploader" style={{color: uploaderColor}}>
                        {this.props.uploader}
                    </div>
                    {!this.state.mobile ? <div className="table-cell col col-time">
                        {formatTime(this.props.time)}
                    </div> : ''}
                </a>
                <div className="table-cell col-actions" onClick={this.openReportForm}>
                    Poista
                </div>
            </div>
        )
    }
    openReportForm() {
        console.log('open report form')
        this.props.openReportForm(this.props.url);
    }
    embedItem() {
        let url = this.props.url;
        this.props.setStreamableFrameUrl(url);
    }
    updateWindowDimensions() {
        let isMobile = window.innerWidth <= 800;

        if (this.state.mobile != isMobile) {
            this.setState({mobile: isMobile});
        }
    }
    componentDidMount() {
        window.addEventListener('resize', this.updateWindowDimensions);
    }
}

export default connect(null, { setStreamableFrameUrl, openReportForm })(ListItem);