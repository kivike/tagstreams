import React, { Component } from 'react';
/*import { setStreamableFrameUrl } from '../../redux/actions/actions';
import { connect } from 'react-redux';*/
import ListItem from './ListItem';

class ListItems extends Component {
    constructor(props) {
        super();
        this.state = {
            sortedStreamables: this.sortStreamables(props.streamables, props.sortBy, props.sortDir),
            windowWidth: props.windowWidth,
            windowHeight: props.windowHeight
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            sortedStreamables: this.sortStreamables(nextProps.streamables, nextProps.sortBy, nextProps.sortDir)
        });
    }

    sortStreamables(streamables, sortBy, sortDir) {
        if (streamables === undefined) {
            return [];
        }
            
        return streamables.sort(function(a, b) {
            return sortDir === 'ASC' ? a[sortBy].localeCompare(b[sortBy]) : b[sortBy].localeCompare(a[sortBy]);
        });
    }
    render() {
        const streamables = this.state.sortedStreamables
            .map((el, index) => 
                <ListItem 
                    key={index}
                    {...el}
                    windowWidth={this.state.windowWidth}
                    windowHeight={this.state.windowHeight} />
            );
        
        return (
            [...streamables]
        )

    }
}

export default ListItems;