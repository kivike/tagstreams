import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getStreamables } from '../../redux/actions/actions';

import ListHeader from './ListHeader';
import ListItems from './ListItems';

class List extends Component {
    constructor(props) {
        super();
        this.state = {
            sortBy: 'time',
            sortDir: 'DESC',
            filteredStreamables: props.streamables,
            mobile: window.innerWidth <= 800
        }
        this.setSortBy = this.setSortBy.bind(this);
        this.submitSearch = this.submitSearch.bind(this);
        this.updateSearch = this.updateSearch.bind(this);
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }

    componentWillMount() {
        this.props.getStreamables(this.state.getParams);
    }
    componentDidMount() {
        window.addEventListener('resize', this.updateWindowDimensions);
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            filteredStreamables: nextProps.streamables
        });
    }
    render() {
        return (
            <div className="streamable-list-container no-select">
                <div className="form-container streamable-search-container" action=''>
                    <input id="streamable-search" onChange={this.updateSearch} placeholder="Etsi"></input>
                </div>
                
                { this.state.filteredStreamables.length ? (
                    <div className="table">
                        <div className="table-row streamable-list-header">
                            { /*!this.state.mobile ? <ListHeader col='url' text='Linkki' onClick={this.setSortBy}/> :''*/ }
                            {<ListHeader col='name' text='Nimi' onClick={this.setSortBy}/>}
                            {/*<ListHeader col='tags' text='Tagit' onClick={this.setSortBy}/>*/}
                            <ListHeader col='uploader' text='Lähettäjä' onClick={this.setSortBy}/>
                            { !this.state.mobile ? <ListHeader col='time' text='Aika' onClick={this.setSortBy} /> : '' }
                        </div>
                        <ListItems
                            windowWidth={this.state.windowWidth}
                            windowHeight={this.state.windowHeight}
                            sortBy={this.state.sortBy}
                            sortDir={this.state.sortDir}
                            streamables={this.state.filteredStreamables}
                        />
                </div>
                ) : (
                    <div>Ei hakutuloksia</div>
                )}
            </div>
        );
    }
    updateWindowDimensions() {
        let isMobile = window.innerWidth <= 800;

        if (this.state.mobile != isMobile) {
            this.setState({mobile: isMobile});
        }
    }
    setSortBy(sortBy) {
        if (this.state.sortBy === sortBy) {
            let newSortDir = this.state.sortDir === 'ASC' ? 'DESC' : 'ASC';
            this.setState({sortDir: newSortDir});
        }
        this.setState({sortBy: sortBy});
    }
    submitSearch(e) {
        e.preventDefault();
        this.updateSearch();
    }
    updateSearch() {
        let filteredStreamables = this.props.streamables.filter(item => {
            const filter = document.getElementById('streamable-search').value.toLowerCase();
            
            const uploader = item.uploader.toLowerCase();
            const name = item.name.toLowerCase();

            let incl = uploader.includes(filter) || name.includes(filter);
            return incl;
        });
        this.setState({
            filteredStreamables: filteredStreamables
        });
    }
}

const mapStateToProps = state => {
    return {
        streamables: formatStreamables(state.streamables.streamables)
    }
}

const formatStreamables = streamables => {
    streamables = streamables.map(function(item) {
        let date = new Date(item.time);

        let year = date.getFullYear();
        let month = '' + (date.getMonth() + 1);
        let day = '' + date.getDate();
        let hours = '' + date.getHours();
        let minutes = '' + date.getMinutes();

        if (month.length < 2) {
            month = '0' + month;
        }
        if (day.length < 2) {
            day = '0' + day;
        }
        if (hours.length < 2) {
            hours = '0' + hours;
        }
        if (minutes.length < 2) {
            minutes = '0' + minutes;
        }
        item.time = year + '-' + month + '-' + day + ' ' + hours + ':' + minutes;
        return item;
    });
    return streamables;
}

export default connect(mapStateToProps, { getStreamables })(List)