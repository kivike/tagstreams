import React from 'react';

const ListHeader = props => {
    const handleClick = () => {
        props.onClick(props.col)
    }
    return (
        <div className={'table-header col-' + props.col} onClick={handleClick}>
            {props.text}
        </div>
    )
}


export default ListHeader;