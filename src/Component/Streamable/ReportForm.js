import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { closeReportForm } from './../../redux/actions/actions';

const UPLOAD_URL_BASE = process.env.NODE_ENV === 'production' ? '/' : 'http://localhost:4444/'
const STREAMABLE_REPORT_URL = UPLOAD_URL_BASE + 'api/streamable/report';

class ReportForm extends Component {

    constructor(props) {
        super();

        this.state = {
            streamableUrl: ''
        }
        this.submitForm = this.submitForm.bind(this);
        this.onBgClick = this.onBgClick.bind(this);
    }
    render() {
        return (
            <div>
                {this.props.streamableUrl ? 
                <div id="report-background" onClick={this.onBgClick}>

                    <form className="form-container report-form-container" action="" onSubmit={this.submitForm}>
                            <h3>
                                Pyydä clipin poistoa
                            </h3>
                            <p>
                                Poistettua clippiä ei pysty lähettämään uudestaan
                            </p>
                            <label>Poiston syy:</label>
                            <textarea id="report-message" max-length="100">

                            </textarea>
                            <button type="submit" id="submit-btn">Lähetä</button>
                        
                    </form>
                </div> : '' }
            </div>
        )
    }
    onBgClick(e) {
        if (e.target.id === 'report-background') {
            this.props.closeReportForm();
        }
    }
    submitForm(e) {
        e.preventDefault();

        const formData = new FormData();

        let clipId = this.props.streamableUrl.split('/').slice(-1)[0];

        formData.append('clip', clipId);
        formData.append('msg', document.getElementById('report-message').value);
        
        axios.post(STREAMABLE_REPORT_URL, formData).then((res) => {
            console.log('reported');
            this.setState({
                sent: true
            });
            this.onUploadComplete(true);
        }).catch((err) => {
            console.log(err);
            this.onUploadComplete(false);
        });
    }
    onUploadComplete(result) {
        this.props.closeReportForm();
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            streamableUrl: nextProps.streamableUrl
        });
    }
}

const mapStateToProps = state => {
    return {
        streamableUrl: state.streamables.reportUrl
    } 
}

export default connect(mapStateToProps, {closeReportForm})(ReportForm);