const mongoose = require('mongoose');

let StreamableSchema = new mongoose.Schema(
    {
        id: Number,
        url: String,
        name: String,
        tags: String,
        uploader: String,
        ip: String,
        disabled: Boolean

    },
    {
        timestamps: { createdAt: 'createdAt' }
    }
)

StreamableSchema.statics.formatObject = function(obj) {
    obj.name = obj.name.substring(0, 50);
    obj.uploader = obj.uploader.substring(0, 20);
    return obj;
}

module.exports = mongoose.model('Streamable', StreamableSchema);
