class Authenticator {
    validatePassword(pw) {
        return pw == process.env.STREAMABLE_PW;
    }
}
module.exports = new Authenticator(); 