const mongoose = require('mongoose');

let StreamableReportSchema = new mongoose.Schema(
    {
        reporter_ip: String,
        streamable_url: String,
        message: String
    },
    {
        timestamps: { createdAt: 'createdAt' }
    }
)

module.exports = mongoose.model('StreamableReport', StreamableReportSchema);