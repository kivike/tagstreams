const Streamable = require('../streamable');

module.exports = {
    getListing: function(page, limit, filter, callback) {
        let query = {};
        let options = {};

        query = { disabled: { $ne: true } }

        if (filter) {
            query.$and = [
                {
                    $or: [
                        {name: '/' + filter + '/i'},
                        {uploader: '/' + filter + '/i'}
                    ],
                },
                query
            ]
            query.$and = [
                {
                    $or: [
                        {name: '/' + filter + '/i'},
                        {uploader: '/' + filter + '/i'}
                    ],
                    disabled: { $ne: true }
                }

            ]
        }
        if (page) {
            options.skip = (page - 1) * limit;
        }
        if (limit) {
            options.limit = limit;
        } else {
            options.limit = 500;
        }
        console.log(query);
        Streamable.find(query, null, options)
        .sort({date: 'desc'})
        .exec(function(err, result) {
            if (result) {
                result = formatListingResult(result);
            }
            callback(err, result);
        });
    }
}

const formatListingResult = function(result) {
    return result.map(function(item) {
        return {
            'url': item.url,
            'name': item.name ? item.name : '',
            'time': item.createdAt,
            'uploader': item.uploader ? item.uploader : '',
            'tags': item.tags ? item.tags : ''
        };
    })
}
