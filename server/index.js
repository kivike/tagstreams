const express = require('express');
const routes = require('./routes/');
const path = require('path');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const auth = require('./auth');
const cookieParser = require('cookie-parser');

require('./db/install');

const PORT = process.env.PORT ? process.env.PORT : '4444';
const DBPORT = process.env.DBPORT ? process.env.DBPORT : '27017';

if (process.env.DBHOST && process.env.DBNAME) {
    var DB_URI = "mongodb://" + process.env.DBHOST + ':' + DBPORT + '/' + process.env.DBNAME;
} else if (process.env.NODE_ENV) {
    var DB_URI = process.env.NODE_ENV === 'production' ?
    "mongodb://192.168.1.62:27017/streamables"
    : "mongodb://192.168.1.62:27017/streamables-dev"
} else {
    console.log('Mongodb not configured');
    process.exit();
}
const app = express();
const router = express.Router();

app.use(cookieParser('245'));
app.use(auth);
app.use(bodyParser.json());
routes(router);

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, token');

    next();
});

app.use('/api', router);

app.use(express.static(path.resolve(__dirname, '..', 'public')));
/*app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '..', 'public', 'index.html'))
});*/

try {
    mongoose.connect(DB_URI, {});
} catch (err) {
    console.log('Failed to connect to database');
    console.log(err);
    process.exit();
}
app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
})


