const index = require('./index.js');
const submitController = require('./../controllers/streamable/submit.ctrl');
const getController = require('./../controllers/streamable/get.ctrl');
const reportController = require('./../controllers/streamable/report.ctrl')

const multipart = require('connect-multiparty');
const multipartWare = multipart();

module.exports = (router) => {
    router.route('/streamable/submit')
        .post(multipartWare, submitController)

    router.route('/streamable')
        .get(getController);

    router.route('/streamable/report')
        .post(multipartWare, reportController)
}