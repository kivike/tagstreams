StreamableReport = require('./../../models/streamable/report');
Streamable = require('./../../models/streamable');

module.exports = (req, res, next) => {
    let { clip, msg } = req.body;

    if (!clip || !msg) {
        res.status(400);
        res.send();
        console.log('Missing arguments for report')
    } else {
        let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress

        let obj = {
            streamable_url: 'https://streamable.com/' + clip,
            message: msg,
            reporter_ip: ip
        }
        
        validateReport(obj)
        .then(function() {
            return saveReport(obj);
        }).then(function() {
            console.log(`Saved report for ${obj.streamable_url}: "${obj.message}"`)
            res.sendStatus(200);
        }).catch(function(err) {
            console.log(err);
            res.sendStatus(500);
        })
    }

}

const validateReport = function(obj) {
    return new Promise(function(resolve, reject) {
        Streamable.find({url: obj.streamable_url}).exec(function(err, result) {
            if (err) {
                reject('Streamable with given URL does not exist');
            } else {
                StreamableReport.find(
                    {
                        streamable_url: obj.streamable_url,
                        reporter_ip: obj.reporter_ip
                    }
                ).exec(function(err, result) {
                    if (err) {
                        reject(err);
                    } else if (result.length) {
                        reject('Duplicate report')
                    } else {
                        resolve();
                    }
                })
            }
        })
    })
}

const saveReport = function(obj) {
    return new Promise(function(resolve, reject) {
        new StreamableReport(obj).save((err, streamable) => {
            if (err) {
                reject(err);
            } else if (!streamable) {
                reject('Could not save streamable');
            } else {
                resolve();
            }
        })
    })
}