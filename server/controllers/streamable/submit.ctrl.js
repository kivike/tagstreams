const Streamable = require('./../../models/streamable');
const Authenticator = require('./../../models/authenticator');
const request = require('request');

const VALIDATE_URL = true;

module.exports = (req, res, next) => {
    console.log('submit streamable');
    
    let {url} = req.body;
    //console.log(password);
    /*if (!Authenticator.validatePassword(password)) {
        console.log('invalid password provided by client');
        res.status(400);
        res.send('Invalid password');
        return;
    }*/

    let obj = {
        url: url,
        name: req.body.name,
        uploader: req.body.uploader,
        tags: req.body.tags,
        ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress
    }

    validateStreamable(obj)
    .then(function() {
        obj = Streamable.formatObject(obj);
        console.log('validation success');
        return saveStreamable(obj);
    }).then(function() {
        console.log('OK 200');
        res.sendStatus(200);
    }).catch(function(err) {
        res.sendStatus(err);
    });
}

const validateStreamable = function(obj) {
    return new Promise(function(resolve, reject) {
        if (!VALIDATE_URL) {
            resolve();
            return;
        }

        if (!obj.url.startsWith('https://streamable.com')) {
            reject(400);
        } else {
            request(obj.url, function(err, response) {
                if (err || response.statusCode != 200) {
                    reject(400)
                } else {
                    Streamable.find({'url': obj.url}).exec(function(err, result) {
                        if (err) {
                            reject(500);
                        } else if (result.length) {
                            reject(400);
                        } else {
                            resolve();
                        }
                    });
                }
            });
        };
    });
}

const saveStreamable = function(obj) {
    return new Promise(function(resolve, reject) {
        console.log(`save ${obj.url}`)
    
        new Streamable(obj).save((err, streamable) => {
            if (err) {
                reject(500)
            } else if (!streamable) {
                reject(500);
            } else {
                resolve();
            }
        });
    })
}