const StreamableList = require('./../../models/streamable/list');

module.exports = (req, res, next) => {
    let {filter, limit, page} = req.body;

    let query = {};
    let options = {};

    if (filter) {
        query.filter = filter;
    }

    if (limit) {
        options.limit = limit;
    }

    StreamableList.getListing(page, limit, filter, function(err, result) {
        if (err) {
            res.status(500);
            res.send(err);
        } else {
            res.send(result);
        }
    })

}