const cookieParser = require('cookie-parser');

module.exports = (req, res, next) => {
    if (!req.signedCookies.user) {
        var authHeader = req.headers.authorization;

        if (!authHeader) {
            res.setHeader('WWW-Authenticate', 'Basic');
            res.sendStatus(401);
            return;
        }

        var auth = new Buffer.from(authHeader.split(" ")[1], 'base64')
            .toString()
            .split(':')

        var username = auth[0];
        var password = auth[1];

        if (username === 'tsuser' && password === '245') {
            res.cookie('user', 'tsuser', {
                signed: true
            });
            next();
        } else {
            res.sendStatus(401);
        }
    } else {
        if (req.signedCookies.user === 'tsuser') {
            next();
        } else {
            res.sendStatus(401);
        }
    }
}

const getNotAuthorizedError = () => {
    let err = new Error('You are not authorized');
    err.status = 401;
    return err;
}