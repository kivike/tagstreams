#!/bin/bash

if [ -z $1 ]; then
	echo 'Missing GIT_URL parameter'
fi
if [ -z $2 ]; then
	echo 'Missing APP_DIR parameter'
fi
if [ -z $3 ]; then
	echo 'Missing COMPOSE_FILE parameter'
fi
GIT_URL=$1
APP_DIR=$2
COMPOSE_FILE=$3

set -x

commit=$(git ls-remote $GIT_URL HEAD | awk '{ print $1 }' | head -c6)

if [ -z $commit ]; then
    echo 'git connection failed'
    exit 1
fi
BUILD_DIR="${APP_DIR}"

if [ ! -d $BUILD_DIR ]; then
	mkdir -p $BUILD_DIR
	cd $BUILD_DIR
	git clone $GIT_URL .
else
	cd $BUILD_DIR
	git pull origin master
fi

docker build -f Dockerfile.prod -t dr.rope.lan:5000/tagstreams:latest .
docker-compose -f $COMPOSE_FILE up -d