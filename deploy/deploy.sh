#!/bin/bash
set -e

ssh-agent

echo "Deploying"
cd $(dirname "$0");

source ./config.sh
echo $APP_DIR

if [[ "$KEYFILE" != "" ]]; then
    KEYARG="-i $KEYFILE"
else
    KEYARG=
fi

COMPOSE_FILE=/home/docker/compose/tagstreams/docker-compose.yml
REMOTE_SCRIPT_PATH=/tmp/build.sh
scp $KEYARG remote/deploy.sh $SERVER:$REMOTE_SCRIPT_PATH
echo
echo "Running deploy script on remote"
ssh -tA $KEYARG $SERVER "bash $REMOTE_SCRIPT_PATH $GIT_URL $APP_DIR $COMPOSE_FILE"